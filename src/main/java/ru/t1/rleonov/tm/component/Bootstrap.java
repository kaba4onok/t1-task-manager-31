package ru.t1.rleonov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.rleonov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.rleonov.tm.api.repository.ICommandRepository;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.repository.IUserRepository;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.command.AbstractCommand;
import ru.t1.rleonov.tm.dto.request.ServerAboutRequest;
import ru.t1.rleonov.tm.dto.request.ServerVersionRequest;
import ru.t1.rleonov.tm.endpoint.SystemEndpoint;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.rleonov.tm.exception.system.CommandNotSupportedException;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.repository.CommandRepository;
import ru.t1.rleonov.tm.repository.ProjectRepository;
import ru.t1.rleonov.tm.repository.TaskRepository;
import ru.t1.rleonov.tm.repository.UserRepository;
import ru.t1.rleonov.tm.service.*;
import ru.t1.rleonov.tm.util.SystemUtil;
import ru.t1.rleonov.tm.util.TerminalUtil;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.rleonov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User user1 = userService.create("user1", "user1", "user1@mail.ru");
        @NotNull final User user2 = userService.create("user2", "user2", "user2@mail.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(user1.getId(), new Project("FIRST PROJECT", Status.COMPLETED));
        projectService.add(user1.getId(), new Project("A SECOND PROJECT", Status.IN_PROGRESS));
        projectService.add(user2.getId(), new Project("THIRD PROJECT", Status.NOT_STARTED));

        taskService.add(user1.getId(), new Task("FIRST TASK", Status.COMPLETED));
        taskService.add(user1.getId(), new Task("A SECOND TASK", Status.IN_PROGRESS));
        taskService.add(user2.getId(), new Task("THIRD TASK", Status.NOT_STARTED));
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        loggerService.info("***WELCOME TO TASK-MANAGER***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("***TASK-MANAGER IS SHUTTING DOWN***");
        backup.stop();
        fileScanner.stop();
        server.stop();
    }

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
        registry(command);
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) throw new CommandNotSupportedException();
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                processCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, final boolean checkRoles) {
        if (command == null) throw new CommandNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArguments(@NotNull final String[] args) {
        @NotNull final String arg = args[0];
        try {
            processArgument(arg);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
        }
    }

    private void processArgument(@Nullable final String arg) {
        if (arg == null) throw new ArgumentNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void run(@Nullable final String[] args) {
        prepareStartup();
        if (args == null || args.length == 0) {
            processCommands();
            return;
        }
        processArguments(args);
    }

}
