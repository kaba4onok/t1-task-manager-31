package ru.t1.rleonov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.Domain;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-xml-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file using fasterxml.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML FASTERXML]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
