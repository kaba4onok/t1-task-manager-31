package ru.t1.rleonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String COMMAND = "project-remove-by-index";

    @NotNull
    private static final String DESCRIPTION = "Remove project by index.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
